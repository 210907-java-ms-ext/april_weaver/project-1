package dev.wea.servlet;

import dev.wea.model.User;
import dev.wea.service.AuthService;
import dev.wea.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResetServlet extends HttpServlet {
    private UserService userService = new UserService();
    private AuthService authService = new AuthService();
    private final Logger logger = Logger.getLogger(ResetServlet.class);

    /**
     * Posts a new password for the User
     *
     * @param req  HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // gets new password and the token
        String passwordParam = req.getParameter("password");
        String paramToken = req.getParameter("token");

        boolean tokenIsValidFormat = authService.validateToken(paramToken);

        if(!tokenIsValidFormat){
            resp.sendError(400, "Improper token format; cannot fulfill your request");
        } else {
            User currentUser = authService.findUserByToken(paramToken);
            if (currentUser == null) {
                resp.sendError(401, "Auth token invalid - no user");
            } else {
                // if there is a valid token and that token is for an admin, return list of users
                resp.setStatus(200);
                userService.updatePassword(currentUser, passwordParam);
                logger.info("Updated "+currentUser.getUsername()+"'s password to "+passwordParam);
            }
        }
    }
}
