<h1>Project 1</h1>
<h2>Project Description</h2>
This is an inidivdual project where it mimmics an expense reimbursement system with employee and manager interactions.

<h2>Technologies Used</h2>
<ul>
<li>Java 1.8</li>
<li>Servlets</li>
<li>JDBC</li>
<li>SQL</li>
<li>HTML</li>
<li>CSS</li>
<li>Javascript</li>
<li>Bootstrap</li>
<li>AJAX</li>
<li>JUnit </li>
<li>log4j</li>
</ul>

<h2>Features</h2>
<ul>
<li>Unique homepage for employee and manager</li>
<li>Manager registers a new employee</li>
<li>Employee can change their password</li>
<li>Manager can view a list of all the employees</li>
<li>Employee can make a request and view all the requests they made</li>
<li>Manager can approve or deny a request and see which manager resolved past requests</li>
<ul>
<br>
<h2>Getting Started</h2>
git clone https://gitlab.com/210907-java-ms-ext/april_weaver/project-1.git
<br>
<br>
Run the script.sql in postgres to set up the tables.
<br>
<br>
Go to http://localhost:8080/Project1/login.html to start.
<h2>Usage</h2>
Be able to send reimbursement requests that will be approved or denied by a manager.

