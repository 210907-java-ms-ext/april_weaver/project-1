package dev.wea.data;

import dev.wea.model.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
class UserDaoTest{

    private UserDao tempDao = new UserDao();

    /**
     * tests the getUserById method with a valid id
     */
    @Test
    void getUserByValidId() {
        User actual = tempDao.getUserById(1);
        User expected = new User(1,"hello", "hi", "Employee", "hello@email.com");
        assertEquals(actual, expected);
    }

    /**
     * tests the getUserById method with an invalid id
     */
    @Test
    void getUserByInvalidId() {
        User actual = tempDao.getUserById(100);
        assertNull(actual);
    }

    /**
     * tests the getUserByValidUsernameAndPassword method with a valid username and password
     */
    @Test
    void getUserByValidUsernameAndPassword() {
        User actual = tempDao.getUserByUsernameAndPassword("hello", "hi");
        User expected = new User(1,"hello", "hi", "Employee", "hello@email.com");
        assertEquals(actual, expected);
    }

    /**
     * tests the getUserByValidUsernameAndPassword method with an invalid username and password
     */
    @Test
    void getUserByInvalidUsernameAndPassword() {
        User actual = tempDao.getUserByUsernameAndPassword("bleh", "ajsdfkjlsdfalkdf");
        assertNull(actual);
    }

    /**
     * tests the checkUsernameExists() with valid and invalid usernames
     */
    @Test
    void checkUsernameExists() {
        Boolean actual = tempDao.checkUsernameExists("hello");
        assertTrue(actual);
        actual = tempDao.checkUsernameExists("11111111");
        assertFalse(actual);
    }
}