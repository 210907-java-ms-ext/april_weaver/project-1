package dev.wea.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RequestTest {

    private Request temp;

    /**
     * sets up Request temp before each test
     */
    @BeforeEach
    void beforeEachSetup() {
        temp = new Request(0, 0,null, null, null, 0);
    }

    /**
     * tests the getter and setter methods for the Request's id
     */
    @Test
    void testGetAndSetId() {
        assertEquals(0, temp.getId());
        int actual = 1;
        temp.setId(actual);
        assertEquals(actual, temp.getId());
    }

    /**
     * tests the getter and setter methods for the Request's subject
     */
    @Test
    void testGetAndSetSubject() {
        assertEquals(null, temp.getSubject());
        String actual = "Hello";
        temp.setSubject(actual);
        assertEquals(actual, temp.getSubject());
    }

    /**
     * tests the getter and setter methods for the Request's id of manager
     */
    @Test
    void testGetAndSetEmployeeID() {
        assertEquals(0, temp.getManager_id());
        int actual = 1;
        temp.setManager_id(actual);
        assertEquals(actual, temp.getManager_id());
    }

    /**
     * tests the getter and setter methods for the Request's id of employee
     */
    @Test
    void testGetAndSetManagerID() {
        assertEquals(0, temp.getEmployee_id());
        int actual = 1;
        temp.setEmployee_id(actual);
        assertEquals(actual, temp.getEmployee_id());
    }

    /**
     * tests the getter and setter methods for the Request's description
     */
    @Test
    void testGetAndSetDescription() {
        assertEquals(null, temp.getDescription());
        String actual = "Hello";
        temp.setDescription(actual);
        assertEquals(actual, temp.getDescription());
    }

    /**
     * tests the getter and setter methods for the Request's status
     */
    @Test
    void testGetAndSetComplete() {
        assertEquals(null, temp.getComplete());
        String actual = "Approved";
        temp.setComplete(actual);
        assertEquals(actual, temp.getComplete());
    }

    /**
     * tests the overridden equals and hashcode methods
     */
    @Test
    void testHashCodeAndEquals() {
        Request first = new Request(0, 0,null, null, null, 0);
        Request second = new Request(0, 0,null, null, null, 0);
        assertTrue(second.equals(first) && first.equals(second));
        assertTrue(first.hashCode() == second.hashCode());

        first.setId(10);

        assertTrue(!second.equals(first) && !first.equals(second));
        assertTrue(first.hashCode() != second.hashCode());
    }

    /**
     * tests the overridden toString method
     */
    @Test
    void testToString() {
        String actual = "Request{id=0, employee_id=0, description='null', complete='null', manager_id=0, subject='null'}";
        assertEquals(actual, temp.toString());

        temp.setId(10);
        actual = "Request{id=10, employee_id=0, description='null', complete='null', manager_id=0, subject='null'}";
        assertEquals(actual, temp.toString());
    }
}