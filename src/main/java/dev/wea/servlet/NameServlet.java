package dev.wea.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.wea.model.User;
import dev.wea.service.AuthService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class NameServlet extends HttpServlet {
    private AuthService authService = new AuthService();
    private final Logger logger = Logger.getLogger(NameServlet.class);

    /**
     * Gets the name of the User
     *
     * @param req HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get token from the header
        String authToken = req.getHeader("Authorization");

        // check token is actually a token
        boolean tokenIsValidFormat = authService.validateToken(authToken);
        if(!tokenIsValidFormat){
            resp.sendError(400, "Improper token format; cannot fulfill your request");
        } else {
            User currentUser = authService.findUserByToken(authToken);
            if (currentUser == null) {
                resp.sendError(401, "Auth token invalid - no user");
            } else {
                // return list of users
               resp.setStatus(200);
                try(PrintWriter pw = resp.getWriter();){
                    User user = authService.findUserByToken(authToken);
                    ObjectMapper om = new ObjectMapper();
                    String userJson = om.writeValueAsString(user);
                    pw.write(userJson);
                    logger.info("Got the name of current user: "+currentUser.getUsername());
                }
            }
        }
    }
}
