package dev.wea.service;

import dev.wea.data.UserDao;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AuthServiceTest {
    AuthService tempService = new AuthService();

    /**
     * tests the validateToken method
     */
    @Test
    void validateToken() {
        Boolean test1 = tempService.validateToken("1:Employee");
        Boolean test2 = tempService.validateToken("1:Manager");
        Boolean test3 = tempService.validateToken("a:Employee");
        Boolean test4 = tempService.validateToken("1:Easdfasfd");
        Boolean test5 = tempService.validateToken("asfas:dfsa:fd");

        assertAll(
                ()->assertTrue(test1),
                ()->assertTrue(test2),
                ()->assertFalse(test3),
                ()->assertFalse(test4),
                ()->assertFalse(test5)
        );
    }

    /**
     * tests the findUserByToken method
     */
    @Test
    void findUserByToken() {
        assertEquals(tempService.findUserByToken("1:Employee"), new UserDao().getUserById(1));

        assertNull(tempService.findUserByToken("10000:Employee"));
    }
}