package dev.wea.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.wea.model.Request;
import dev.wea.model.User;
import dev.wea.service.AuthService;
import dev.wea.service.RequestService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class RequestServlet extends HttpServlet {
    private AuthService authService = new AuthService();
    private RequestService requestService = new RequestService();
    private final Logger logger = Logger.getLogger(RequestServlet.class);

    /**
     * Gets the requests based on the token
     *
     * @param req HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // receives the token from the header
        String authToken = req.getHeader("Authorization");

        // checks the format of the token
        boolean tokenIsValidFormat = authService.validateToken(authToken);
        if(!tokenIsValidFormat){
            resp.sendError(400, "Improper token format; cannot fulfill your request");
        } else {
            User currentUser = authService.findUserByToken(authToken);
            if(currentUser==null){
                resp.sendError(401, "Auth token invalid - no user");
            } else {
                List<Request> requests;
                // if Manager, responds with all the requests
                // if Employee, responds with requests that only belong to the Employee
                if (currentUser.getUserRole().equals("Manager")) {
                    resp.setStatus(200);
                    try(PrintWriter pw = resp.getWriter()) {
                        requests = requestService.getAllRequests();
                        ObjectMapper om = new ObjectMapper();
                        String userJson = om.writeValueAsString(requests);
                        pw.write(userJson);
                        logger.info("Got all the requests: " + requests);
                    }
                } else {
                    resp.setStatus(200);
                    try(PrintWriter pw = resp.getWriter()) {
                        // write to response body
                        requests = requestService.getRequestsByID(currentUser.getId());
                        ObjectMapper om = new ObjectMapper(); // ObjectMapper utilizes Java Reflection to introspect
                        // on the user object to create my json
                        String userJson = om.writeValueAsString(requests);
                        om.writeValue(pw, requests);
                        //pw.write(userJson);
                        logger.info("Got all the requests for employee "+ currentUser.getUsername());
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                    }
                }
            }
        }
    }

    /**
     * Posts a request into the database
     *
     * @param req HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get information from request
        String subjectParam = req.getParameter("subject");
        String descriptionParam = req.getParameter("description");
        String token = req.getParameter("token");

        User currentUser = authService.findUserByToken(token); // return null if none found

        // posts the request when the user is found
        if(currentUser!=null){
            logger.info("Request posted");
            requestService.addNewRequest(currentUser.getId(), subjectParam, descriptionParam);
            resp.setStatus(200);
        } else {
            resp.sendError(401, "Auth token invalid - no user");
        }
    }
}
