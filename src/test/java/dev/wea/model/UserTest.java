package dev.wea.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    private User temp;

    /**
     * sets up User temp before each test
     */
    @BeforeEach
    void beforeEachSetup() {
        temp = new User(0, null, null, null, null);
    }

    /**
     * tests the getter and setter methods for the User's id
     */
    @Test
    void testGetAndSetId() {
        assertEquals(0, temp.getId());
        int actual = 1;
        temp.setId(actual);
        assertEquals(actual, temp.getId());
    }

    /**
     * tests the getter and setter methods for the User's username
     */
    @Test
    void testGetAndSetUsername() {
        assertNull(temp.getUsername());
        String actual = "username";
        temp.setUsername(actual);
        assertEquals(actual, temp.getUsername());
    }

    /**
     * tests the getter and setter methods for the User's email
     */
    @Test
    void testGetAndSetEmail() {
        assertNull(temp.getEmail());
        String actual = "@email.com";
        temp.setEmail(actual);
        assertEquals(actual, temp.getEmail());
    }

    /**
     * tests the getter and setter methods for the User's password
     */
    @Test
    void testGetAndSetPassword() {
        assertNull(temp.getPassword());
        String actual = "username";
        temp.setPassword(actual);
        assertEquals(actual, temp.getPassword());
    }

    /**
     * tests the getter and setter methods for the User's role
     */
    @Test
    void testGetAndSetUserRole() {
        assertNull(temp.getUserRole());
        String actual = "Employee";
        temp.setUserRole(actual);
        assertEquals(actual, temp.getUserRole());
    }

    /**
     * tests the overridden equals and hashcode methods
     */
    @Test
    void testHashCodeAndEquals() {
        User first = new User(0, null, null, null, null);
        assertTrue(temp.equals(first) && first.equals(temp));
        assertTrue(first.hashCode() == temp.hashCode());

        first.setUsername("username");

        assertTrue(!temp.equals(first) && !first.equals(temp));
        assertTrue(first.hashCode() != temp.hashCode());
    }

    /**
     * tests the overridden toString method
     */
    @Test
    void testToString() {
        String actual = "User [id=0, username=null, password=null, userRole=null, email=null]";
        assertEquals(actual, temp.toString());

        temp.setUsername("username");
        actual = "User [id=0, username=username, password=null, userRole=null, email=null]";
        assertEquals(actual, temp.toString());
    }
}