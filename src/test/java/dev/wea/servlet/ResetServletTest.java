package dev.wea.servlet;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.mockito.Mockito.*;

class ResetServletTest {
    private HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    private HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    /**
     * tests the doPost method
     *
     * @throws Exception
     */
    @Test
    void doPost() throws Exception{
        when(request.getParameter("password")).thenReturn("hi");
        when(request.getParameter("token")).thenReturn("1:Employee");

        new ResetServlet().doPost(request, response);

        verify(request, atLeast(1)).getParameter("password");
        verify(request, atLeast(1)).getParameter("token");
        verify(response, atLeast(1)).setStatus(200);
    }
}