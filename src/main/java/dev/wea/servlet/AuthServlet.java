package dev.wea.servlet;

import dev.wea.model.User;
import dev.wea.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthServlet extends HttpServlet {
    private UserService userService = new UserService();
    private final Logger logger = Logger.getLogger(AuthServlet.class);

    /**
     * Receives username and password. Prints out a token [id:role] if the user exists in database
     *
     * @param req HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get username and password
        String usernameParam = req.getParameter("username");
        String passwordParam = req.getParameter("password");

        // check to see if user is on database
        User user = userService.getUserByCredentials(usernameParam, passwordParam);

        // returns 401 error or token if user exists
        if(user == null){
            resp.sendError(401, "User credentials provided did not return a valid account");
        } else {
            resp.setStatus(200);
            String token = user.getId() + ":" + user.getUserRole();
            logger.info("Token made for "+usernameParam+": "+token);
            resp.setHeader("Authorization", token);
        }
    }
}
