package dev.wea.servlet;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;
import static org.mockito.Mockito.*;

class UserServletTest {
    private HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    private HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    /**
     * tests the doGet method
     *
     * @throws Exception
     */
    @Test
    void doGet() throws Exception {
        when(request.getHeader("Authorization")).thenReturn("2:Manager");

        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        new RequestServlet().doGet(request, response);

        verify(request, atLeast(1)).getHeader("Authorization");
        writer.flush();
        verify(response, atLeast(1)).setStatus(200);
    }
}