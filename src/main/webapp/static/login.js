// respond to clicking the login button
document.getElementById("login-btn").addEventListener("click", attemptLogin);

function attemptLogin(){
    const errorDiv = document.getElementById("error-div");
    errorDiv.hidden = true;

    // get input values from input fields (username and password)
    const username = document.getElementById("username-input").value;
    const password = document.getElementById("password-input").value;

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/Project1/login");

    // check for readystate 4
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4){
            // look at status code (either 401 or 200)
            // if the status code is 401 - indicate to the user that their credentials are invalid
            if(xhr.status===401){
                errorDiv.hidden = false;
                errorDiv.innerText = "You have entered an invalid username or password";
            } else if (xhr.status===200){
                const token = xhr.getResponseHeader("Authorization");
                sessionStorage.setItem("token", token);

                const tokenSplit = token.split(":");

                //go to admin home if manager, employee home if not
                if( tokenSplit[1] === 'Manager') {
                    window.location.href="/Project1/static/admin-home.html";
                }
                else {
                    window.location.href="/Project1/static/home.html";
                }
            } else {
                errorDiv.hidden = false;
                errorDiv.innerText = "unknown error";
            }
        }
    }

    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    // send request, with the username and password in the request body
    const requestBody = `username=${username}&password=${password}`;
    xhr.send(requestBody);
}
