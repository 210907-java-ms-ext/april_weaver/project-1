package dev.wea.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.wea.model.User;
import dev.wea.service.AuthService;
import dev.wea.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class UserServlet extends HttpServlet {
    private AuthService authService = new AuthService();
    private UserService userService = new UserService();
    private final Logger logger = Logger.getLogger(UserServlet.class);

    /**
     * Gets a list of users if the User is a Manager
     *
     * @param req HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get token from header
        String authToken = req.getHeader("Authorization");

        boolean tokenIsValidFormat = authService.validateToken(authToken);
        if(!tokenIsValidFormat){
            resp.sendError(400, "Improper token format; cannot fulfill your request");
        } else {
            User currentUser = authService.findUserByToken(authToken);
            if(currentUser==null){
                resp.sendError(401, "Auth token invalid - no user");
            } else {
                // returns list of users if the role is Manager
                if(currentUser.getUserRole().equals("Manager")){
                    resp.setStatus(200);
                    try(PrintWriter pw = resp.getWriter();){
                        List<User> users = userService.getAllEmployees();
                        ObjectMapper om = new ObjectMapper();
                        String userJson = om.writeValueAsString(users);
                        pw.write(userJson);
                    }
                    logger.info("Sent list of users to "+currentUser.getUsername());
                } else {
                    resp.sendError(403, "Invalid user role for current request");
                }
            }
        }
    }
}
