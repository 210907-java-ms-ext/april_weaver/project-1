package dev.wea.data;

import dev.wea.model.Request;
import dev.wea.service.ConnectionService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RequestDao {
    private ConnectionService connection;

    /**
     * Constructor method of UserDao class
     * by setting up a connection to the database
     */
    public RequestDao() {
        this.connection = new ConnectionService();
    }

    /**
     * @param employee_id User (Employee) that made the request
     * @param subject subject of the Request
     * @param description description of the Request
     */
    public void addRequest(int employee_id, String subject, String description) {
        try {
            String sql = "insert into request(employee_id, subject, description, complete, manager_id) values (" + employee_id + ", '" + subject + "', '"+ description + "', 'Pending', 0)";
            Connection c = connection.establishConnection();
            Statement s = c.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param request_id Request that its status will be changed
     * @param status The new status of the Request (Approved or Denied)
     * @param manager_id User (Manager) that resolved the Request
     */
    public void updateStateOfRequest(int request_id, String status, int manager_id) {
        try {
            String sql = "update request set complete = '"+status+"', manager_id = "+manager_id+" where req_id = "+request_id;
            Connection c = connection.establishConnection();
            Statement s = c.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return list of requests
     */
    public List<Request> getRequests() {
        List<Request> list = new ArrayList<>();
        try {
            String sql = "select * from request";
            Connection c = connection.establishConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while(rs.next()) {
                Request temp = new Request(rs.getInt("req_id"), rs.getInt("employee_id"), rs.getString("subject"),
                        rs.getString("description"), rs.getString("complete"), rs.getInt("manager_id"));
                list.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * @param id id of a User (Employee)
     * @return list of requests that belong to the User (Employee)
     */
    public List<Request> getRequestsID(int id) {
        List<Request> list = new ArrayList<>();
        try {
            String sql = "select * from request where employee_id =  ?";
            Connection c = connection.establishConnection();
            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1, id);
            ResultSet rs = s.executeQuery();
            while(rs.next()) {
                Request temp = new Request(rs.getInt("req_id"), rs.getInt("employee_id"), rs.getString("subject"),
                        rs.getString("description"), rs.getString("complete"), rs.getInt("manager_id"));
                list.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
