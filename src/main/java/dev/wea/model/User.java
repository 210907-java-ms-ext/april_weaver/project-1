package dev.wea.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String username;
    private String email;

    @JsonIgnore // ObjectMapper ignores this field when converting to JSON
    private String password;
    private String userRole;


    /**
     * Constructor method for the User class
     *
     * @param id serial number of the User
     * @param username unique name of the User
     * @param password password that belongs to the User
     * @param userRole Employee or Manager
     * @param email email of the User
     */
    public User(int id, String username, String password, String userRole, String email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.userRole = userRole;
        this.email = email;
    }

    /**
     * @return email of the User
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email email of the User
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param userRole role of the User
     */
    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    /**
     * @return id of the User
     */
    public int getId() {
        return id;
    }

    /**
     * @param id serial number of the User
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return username of the User
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username unique name of the User
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return password of the User
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password password that belongs to the User
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return finds whether User is Employee or Manager
     */
    public String getUserRole() {
        return userRole;
    }

    /**
     * Overrides the hashcode method
     *
     * @return hashcode of the User object
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((userRole == null) ? 0 : userRole.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        return result;
    }

    /**
     * Overrides the equals method
     *
     * @param obj second User to compare
     * @return boolean on whether two Users are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (id != other.id)
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (userRole == null) {
            if (other.userRole != null)
                return false;
        } else if (!userRole.equals(other.userRole))
            return false;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        return true;
    }

    /**
     * Overrides the toString method
     *
     * @return String of the User
     */
    @Override
    public String toString() {
        return "User [id=" + id + ", username=" + username + ", password=" + password + ", userRole=" + userRole + ", email=" + email + "]";
    }

}
