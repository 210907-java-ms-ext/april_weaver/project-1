package dev.wea.servlet;

import dev.wea.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegisterServlet extends HttpServlet {
    private UserService userService = new UserService();
    private final Logger logger = Logger.getLogger(RegisterServlet.class);

    /**
     * Posts a new User into the database
     *
     * @param req HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get information from request
        String usernameParam = req.getParameter("username");
        String emailParam = req.getParameter("email");
        String passwordParam = req.getParameter("password");
        String roleParam = req.getParameter("role");

        // checks username already exists
        Boolean check = userService.userExist(usernameParam);

        if(!check){
            // adds new User
            logger.info("Created user");
            userService.addNewUser(usernameParam, passwordParam, roleParam, emailParam);
            resp.setStatus(200);

        } else {
            // sends error when username exists
            logger.info("Username Already Exists");
            resp.sendError(401, "Username already exists");
        }
    }
}
