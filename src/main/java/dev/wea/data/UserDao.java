package dev.wea.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dev.wea.model.User;
import dev.wea.service.ConnectionService;

public class UserDao {

    private ConnectionService connection;

    /**
     * Constructor method of UserDao class
     * by setting up a connection to the database
     */
    public UserDao() {
        this.connection = new ConnectionService();
    }

    /**
     * @return list of employees
     */
    public List<User> getEmployees() {
        List<User> users = new ArrayList<>();
        try {
            String sql = "select * from project1_user";
            Connection c = connection.establishConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while(rs.next()) {
                User temp = new User(rs.getInt("user_id"), rs.getString("un"),
                        rs.getString("pw"), rs.getString("user_role"), rs.getString("email"));
                users.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    /**
     * @param id unique number of a User
     * @return User that possesses the id
     */
    public User getUserById(int id) {
        User user = null;
        try {
            String sql = "select * from project1_user where user_id = " + id;
            Connection c = connection.establishConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            if (rs.next()) {
                user = new User(rs.getInt("user_id"), rs.getString("un"),
                        rs.getString("pw"), rs.getString("user_role"), rs.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * @param username username of a User
     * @param password password of a User
     * @return User that posses the username and password
     */
    public User getUserByUsernameAndPassword(String username, String password) {
        User user = null;
        try {
            String sql = "select * from project1_user where un = '" + username + "' AND pw = '" + password + "'";
            Connection c = connection.establishConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            if (rs.next()) {
                user = new User(rs.getInt("user_id"), rs.getString("un"),
                        rs.getString("pw"), rs.getString("user_role"), rs.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * @param username username of the User
     * @param password password of the User
     * @param role role of the Username
     * @param email email that belongs to the User
     */
    public void addUser(String username, String password, String role, String email) {;
        try {
            String sql = "insert into project1_user(un, pw, user_role, email) values ('" + username + "', '" + password + "', '" + role + "', '" + email + "')";
            Connection c = connection.establishConnection();
            Statement s = c.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param username username of a potential User
     * @return true if the username exists within the database, false otherwise
     */
    public Boolean checkUsernameExists(String username) {
        Boolean result = false;
        try {
            String sql = "select exists (select true from project1_user where un = '"+username+"')";
            Connection c = connection.establishConnection();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            rs.next();
            Boolean r = rs.getBoolean("exists");
            if (r) {
                result = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param user a User
     * @param password new password for the User
     */
    public void updateUserPassword(User user, String password) {
        int id = user.getId();
        try {
            String sql = "update project1_user set pw = '"+password+"' where user_id = "+id;
            Connection c = connection.establishConnection();
            Statement s = c.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
