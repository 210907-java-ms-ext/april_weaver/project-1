package dev.wea.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionService {
    private Connection connection;

    public ConnectionService() {
    }

    /**
     * @return the connection to postgres
     */
    public Connection establishConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                Class.forName("org.postgresql.Driver");
                String url = System.getenv("Postgres_url");
                String username = "postgres";
                String password = "password";
                connection = DriverManager.getConnection(url, username, password);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
