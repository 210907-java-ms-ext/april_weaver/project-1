package dev.wea.model;

import java.io.Serializable;
import java.util.Objects;

public class Request implements Serializable {

    private int id;
    private int employee_id;
    private String description;
    private String complete;
    private int manager_id;
    private String subject;

    /**
     * Constructor method of Request class
     *
     * @param id serial number of the Request
     * @param employee_id Employee that submitted the request
     * @param subject subject of the Request
     * @param description description of the Request
     * @param complete Pending, Approved, or Denied
     * @param manager_id Manager that resolved the Request (Approved or Denied)
     */
    public Request(int id, int employee_id, String subject, String description, String complete, int manager_id) {
       this.id = id;
       this.employee_id = employee_id;
       this.description = description;
       this.complete = complete;
       this.manager_id = manager_id;
       this.subject = subject;
    }

    /**
     * @return employee that submitted the Request
     */
    public int getEmployee_id() {
        return employee_id;
    }

    /**
     * @param employee_id employee that submitted the Request
     */
    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    /**
     * @return status of the Request
     */
    public String getComplete() {
        return complete;
    }

    /**
     * @param complete status of the Request
     */
    public void setComplete(String complete) {
        this.complete = complete;
    }

    /**
     * @return manager that resolved the Request
     */
    public int getManager_id() {
        return manager_id;
    }

    /**
     * @param manager_id manager that resolved the Request
     */
    public void setManager_id(int manager_id) {
        this.manager_id = manager_id;
    }

    /**
     * @return subject of the Request
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject subject of the Request
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return description of the Request
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description description of the Request
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return id of the Request
     */
    public int getId() {
        return id;
    }

    /**
     * @param id serial number of the Request
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Overrides the equals method
     *
     * @param o second Request to compare
     * @return boolean on whether two Requests are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Request request = (Request) o;
        if (id != request.id)
            return false;
        if (employee_id != request.employee_id)
            return false;
        if (manager_id != request.manager_id)
            return false;
        if (subject == null) {
            if (request.subject != null)
                return false;
        } else if (!subject.equals(request.subject))
            return false;
        if (description == null) {
            if (request.description != null)
                return false;
        } else if (!description.equals(request.description))
            return false;
        if (complete == null) {
            return request.complete == null;
        } else return complete.equals(request.complete);
    }

    /**
     * Overrides the hashcode method
     *
     * @return hash code of the Request
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, employee_id, description, complete, manager_id, subject);
    }

    /**
     * Overrides the toString method
     *
     * @return string of the Request
     */
    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", employee_id=" + employee_id +
                ", description='" + description + '\'' +
                ", complete='" + complete + '\'' +
                ", manager_id=" + manager_id +
                ", subject='" + subject + '\'' +
                '}';
    }
}