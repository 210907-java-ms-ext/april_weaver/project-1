package dev.wea.service;

import dev.wea.data.RequestDao;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RequestServiceTest {
    RequestService tempService = new RequestService();

    /**
     * tests the getAllRequests method
     */
    @Test
    void getAllRequests() {
        assertEquals(tempService.getAllRequests(), new RequestDao().getRequests());
    }

    /**
     * tests the getRequestsByID method
     */
    @Test
    void getRequestsByID() {
        assertEquals(tempService.getRequestsByID(1), new RequestDao().getRequestsID(1));
    }
}