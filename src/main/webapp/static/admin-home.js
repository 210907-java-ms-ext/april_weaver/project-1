//build list of employees
let displayUsers = users => {
    const employeeContent = document.getElementById("employee-content");
    const userList = document.createElement("ul");
    for(let user of users){
        const userListItem = document.createElement("li");
        var tempArray = [user.username, user.id]
        if (user.userRole == "Employee") {
            userListItem.innerText = `${user.username}:\u00a0\u00a0\u00a0\u00a0${user.email}`;
            employeeList.push(tempArray);
            userList.appendChild(userListItem);
        } else {
            managerList.push(tempArray);
        }
    }
    employeeContent.appendChild(userList);
}

//build list of request and store into variable
let makeList = requests => {
    const requestContent = document.getElementById("request-list");
    for(let request of requests){
        var array = [request.subject, request.complete, request.description, request.employee_id, request.manager_id, request.id];
        requestList.unshift(array);
    }
}

const token = sessionStorage.getItem("token");
var requestList = [];
var employeeList = [];
var managerList = [];

//retrieve employees from users database
if(token){
    fetch("/Project1/name", {headers: {"Authorization" : token}})
    .then(response => response.json())
    .then((responseJson) => {
        document.getElementById("home-content").innerHTML = "Welcome " + responseJson.username;
    });

    fetch("/Project1/users", {headers: {"Authorization" : token}})
    .then(response => response.json()) //converts JSON response body into JS objects
    .then(displayUsers);

    fetch("/Project1/request", {headers: {"Authorization" : token}})
            .then(response => response.json()) //converts JSON response body into JS objects
            .then(makeList);
} else {
    window.location.href = "/Project1/static/login.html"
}

// logout by clicking on the button (back to the login page)
document.getElementById("logout-btn").addEventListener("click", attemptLogout);
function attemptLogout(){
    window.location.href="/Project1/static/login.html";
}

//listen for the register button
document.getElementById("form").addEventListener('submit', attemptRegister);

function attemptRegister(e) {

    e.preventDefault();

    const errorDiv = document.getElementById("error-div");
    errorDiv.style.display = 'none';

    const username = document.getElementById("username-input").value;
    const password = document.getElementById("temp-password").value;
    const email = document.getElementById("email-input").value;
    const role = "Employee";

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/Project1/register");

    xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                // look at status code (either 401 or 200)
                // if the status code is 401 - indicate to the user that their credentials are invalid
                if(xhr.status===200){
                    location.reload();
                    return false;
                } else if (xhr.status===401) {
                    errorDiv.style.display = '';
                }
        }
    }

        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        // send request, with the username and password in the request body
        const requestBody = `username=${username}&password=${password}&email=${email}&role=${role}`;
        xhr.send(requestBody);
}

//display requests based on input
function viewRequests(y) {
    const requestContent = document.getElementById("request-list");
    requestContent.innerHTML = ""; //clears list

    if (y === "1") {
        requestList.forEach(function (item, index) {
            let btn = document.createElement("button");
            var complete;
            btn.innerHTML = item[0] + ": " + item[1];
            btn.setAttribute("onclick", "display(this.value)");
            btn.setAttribute("class","btn-outline-dark");
            btn.setAttribute("style","width: 75%; height: 35px");
            btn.setAttribute("value",index);
            requestContent.appendChild(btn);
        });
    } else if (y === "2") {
        requestList.forEach(function (item, index) {
            if (item[1] != "Pending") {
                let btn = document.createElement("button");
                btn.innerHTML = item[0] + ": " + item[1];
                btn.setAttribute("onclick", "display(this.value)");
                btn.setAttribute("class","btn-outline-dark");
                btn.setAttribute("style","width: 75%; height: 35px");
                btn.setAttribute("value",index);
                requestContent.appendChild(btn);
            }
        });
    } else if (y === "3") {
        requestList.forEach(function (item, index) {
            if (item[1] == "Pending") {
                let btn = document.createElement("button");
                btn.innerHTML = item[0] + ": " + item[1];
                btn.setAttribute("onclick", "display(this.value)");
                btn.setAttribute("class","btn-outline-dark");
                btn.setAttribute("style","width: 75%; height: 35px");
                btn.setAttribute("value",index);

                requestContent.appendChild(btn);
            }
        });
    }
}

function display(z) {
    const currentRequest = requestList[z];
    var employeeUsername;
    document.getElementById("request-content-s").innerHTML = "<b>Subject: </b>" + currentRequest[0];
    for (var i = 0; i < employeeList.length; i++) {
        var temp = employeeList[i];
        if (temp[1] == currentRequest[3]) {
            employeeUsername = temp[0];
        }
    }
    document.getElementById("request-content-e").innerHTML = "<b>Employee: </b>" + employeeUsername;
    document.getElementById("request-content-c").innerHTML = "<b>Status: </b>" + currentRequest[1];
    document.getElementById("request-content-d").innerHTML = "<hr><h5><b>Description: </b></h5>" + currentRequest[2];

    if (currentRequest[1] == "Pending") {
        const twoButtons = document.getElementById("request-content-f");
        document.getElementById("request-content-f").innerHTML = "";

        let btn1 = document.createElement("button");
        btn1.innerHTML = "Approve";
        btn1.setAttribute("onclick", "completeRequest(event, this.value, "+currentRequest[5]+")");
        btn1.setAttribute("class","btn btn-success btn-block");
        btn1.setAttribute("style","background-color: #000000; color: #FFFFFF; margin-right: 10px;");
        btn1.setAttribute("value","Approved");
        twoButtons.appendChild(btn1);

        let btn2 = document.createElement("button");
        btn2.innerHTML = "Deny";
        btn2.setAttribute("onclick", "completeRequest(event, this.value, "+currentRequest[5]+")");
        btn2.setAttribute("class","btn btn-success btn-block");
        btn2.setAttribute("style","background-color: #000000; color: #FFFFFF");
        btn2.setAttribute("value", "Denied");
        twoButtons.appendChild(btn2);

    } else {
        var managerUsername;
        for (var i = 0; i < managerList.length; i++) {
                var temp = managerList[i];
                if (temp[1] == currentRequest[4]) {
                    managerUsername = temp[0];
                }
            }
        document.getElementById("request-content-f").innerHTML = "<b>"+currentRequest[1]+" by manager "+managerUsername+"</b>";
    }
}

function completeRequest(e, s, paramid) {

    e.preventDefault();

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/Project1/update");

    xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                // look at status code (either 401 or 200)
                // if the status code is 401 - indicate to the user that their credentials are invalid
                if(xhr.status===200){
                    location.reload();
                    return false;
                }
        }
    }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        // send request, with the username and password in the request body
        const requestBody = `id=${paramid}&state=${s}&token=${token}`;
        xhr.send(requestBody);
}