package dev.wea.service;

import dev.wea.model.User;

public class AuthService {
    private UserService userService = new UserService();

    /**
     * @param authToken token of the User
     * @return true if the token is in the format of int:String
     *         with String either being Employee or Manager, false otherwise
     */
    public boolean validateToken(String authToken){
        if(authToken==null){
            return false;
        }

        // make sure there is only two values in the array
        String[] tokenArr = authToken.split(":");
        if(tokenArr.length != 2){
            return false;
        }

        // make sure the first value is numeric
        String idString = tokenArr[0];
        if(!idString.matches("^\\d+$")){
            return false;
        }

        // make sure the second value is "Manager" or "Employee"
        String roleString = tokenArr[1];
        String[] roles = new String[]{"Manager", "Employee"};
        for(String role: roles){
            if(role.equals(roleString)){
                return true;
            }
        }
        return false;
    }

    /**
     * @param authToken token of the User
     * @return the User that the token represents or null
     */
    public User findUserByToken(String authToken){
        if (authToken == null) {
            return null;
        }
        String[] tokenArr = authToken.split(":");
        int id = Integer.parseInt(tokenArr[0]);
        return userService.getUserById(id);
    }

}
