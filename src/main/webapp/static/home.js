//check passwords match
function check() {
    if (document.getElementById('repeat-password').value != document.getElementById('password-input').value) {
        document.getElementById("message").innerHTML = 'Passwords must be matching.';
        document.getElementById("password-btn").disabled = true;
    } else {
    // input is valid -- reset the error message
        document.getElementById("message").innerHTML = '';
        document.getElementById("password-btn").disabled = false;
    }
}

//get token
const token = sessionStorage.getItem("token");
const tokenSplit = token.split(":");
var requestList = [];

//build list of request and store into variable
let makeList = requests => {
    const requestContent = document.getElementById("request-list");
    for(let request of requests){
        var array = [request.subject, request.complete, request.description];
        requestList.unshift(array);
    }
}

//update page by what the token stores
if(token){
    //updates the home page
    fetch("/Project1/name", {headers: {"Authorization" : token}})
    .then(response => response.json())
    .then((responseJson) => {
        document.getElementById("home-content").innerHTML = "Welcome " + responseJson.username;
        document.getElementById("profile-content1").innerHTML = "<b>Username:</b> " + responseJson.username;
        document.getElementById("profile-content2").innerHTML = "<b>Email:</b> " + responseJson.email;
    })

    fetch("/Project1/request", {headers: {"Authorization" : token}})
        .then(response => response.json()) //converts JSON response body into JS objects
        .then(makeList);
} else {
    window.location.href = "/Project1/static/login.html"
}

//display requests based on input
function viewRequests(y) {
    const requestContent = document.getElementById("request-list");
    requestContent.innerHTML = ""; //clears list
    const linebreak = document.createElement('br');

    if (y === "1") {
        requestList.forEach(function (item, index) {
            let btn = document.createElement("button");
            var complete;
            btn.innerHTML = item[0] + ": " + item[1];
            btn.setAttribute("onclick", "display(this.value)");
            btn.setAttribute("class","btn-outline-dark");
            btn.setAttribute("style","width: 75%; height: 35px");
            btn.setAttribute("value",index);
            requestContent.appendChild(btn);
        });
    } else if (y === "2") {
        requestList.forEach(function (item, index) {
            if (item[1] != "Pending") {
                let btn = document.createElement("button");
                btn.innerHTML = item[0] + ": " + item[1];
                btn.setAttribute("onclick", "display(this.value)");
                btn.setAttribute("class","btn-outline-dark");
                btn.setAttribute("style","width: 75%; height: 35px");
                btn.setAttribute("value",index);
                requestContent.appendChild(btn);
            }
        });
    } else if (y === "3") {
        requestList.forEach(function (item, index) {
            if (item[1] == "Pending") {
                let btn = document.createElement("button");
                btn.innerHTML = item[0] + ": " + item[1];
                btn.setAttribute("onclick", "display(this.value)");
                btn.setAttribute("class","btn-outline-dark");
                btn.setAttribute("style","width: 75%; height: 35px");
                btn.setAttribute("value",index);

                requestContent.appendChild(btn);
            }
        });
    }
}

function display(z) {
    const currentRequest = requestList[z];
    const subject = currentRequest[0];
    const description = currentRequest[2];
    document.getElementById("request-content-s").innerHTML = "<b>Subject: </b>" + subject;
    document.getElementById("request-content-c").innerHTML = "<b>Status: </b>" + currentRequest[1];
    document.getElementById("request-content-d").innerHTML = "<hr><h5><b>Description: </b></h5>" + description;
}


// logout by clicking on the button (back to the login page)
document.getElementById("logout-btn").addEventListener("click", attemptLogout);
function attemptLogout(){
    window.location.href="/Project1/static/login.html";
}

//submit a request when pressing the submit button
document.getElementById("request-form").addEventListener('submit', attemptRequest);

function attemptRequest(e) {
    e.preventDefault();
    const subject = document.getElementById("subject-input").value;
    const description = document.getElementById("description-input").value;

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/Project1/request");

    xhr.onreadystatechange = function(){
            if(xhr.readyState===4){
                // look at status code (either 401 or 200)
                // if the status code is 401 - indicate to the user that their credentials are invalid
                if(xhr.status===200){
                    location.reload();
                    return false;
                }
        }
    }

    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    // send request, with the username and password in the request body
    const requestBody = `subject=${subject}&description=${description}&token=${token}`;
    xhr.send(requestBody);
}

//reset the password when pressing the submit button
document.getElementById("password-form").addEventListener('submit', attemptReset);

function attemptReset(e) {
    e.preventDefault();

        const password = document.getElementById("password-input").value;

        const xhr = new XMLHttpRequest();
        xhr.open("POST", "/Project1/reset");

        xhr.onreadystatechange = function(){
                if(xhr.readyState===4){
                    // look at status code (either 401 or 200)
                    // if the status code is 401 - indicate to the user that their credentials are invalid
                    if(xhr.status===200){
                        location.reload();
                        return false;
                    }
            }
        }
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            // send request, with the username and password in the request body
            const requestBody = `password=${password}&token=${token}`;
            xhr.send(requestBody);
}