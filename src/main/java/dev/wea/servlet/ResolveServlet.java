package dev.wea.servlet;

import dev.wea.model.User;
import dev.wea.service.AuthService;
import dev.wea.service.RequestService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResolveServlet extends HttpServlet {
    private AuthService authService = new AuthService();
    private RequestService requestService = new RequestService();
    private final Logger logger = Logger.getLogger(ResolveServlet.class);

    /**
     * Posts a new status on a Request
     *
     * @param req HttpServletRequest
     * @param resp HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get information from request
        int idParam = Integer.parseInt(req.getParameter("id"));
        String stateParam = req.getParameter("state");
        String token = req.getParameter("token");

        User currentUser = authService.findUserByToken(token); // return null if none found

        if(currentUser!=null){
            logger.info("Request being updated to "+stateParam);
            requestService.completeRequest(idParam, stateParam, currentUser.getId());
            resp.setStatus(200);
        } else {
            resp.sendError(401, "Auth token invalid - no user");
        }
    }
}
