package dev.wea.service;

import dev.wea.data.UserDao;
import dev.wea.model.User;

import java.util.List;

public class UserService {
    private UserDao userDao = new UserDao();

    /**
     * @return list of Users (Employee)
     */
    public List<User> getAllEmployees(){
        return userDao.getEmployees();
    }

    /**
     * @param username username of a User
     * @param password password of a User
     * @return User that possesses the username and password
     */
    public User getUserByCredentials(String username, String password){
        if(username==null || username.isEmpty() || password == null || password.isEmpty()){
            return null;
        }
        return userDao.getUserByUsernameAndPassword(username, password);
    }

    /**
     * @param id id of a User
     * @return User that possesses the id
     */
    public User getUserById(int id){
        return userDao.getUserById(id);
    }

    /**
     * @param username username of the User
     * @param password password of the User
     * @param role role of the User (Employee)
     * @param email email of the User
     */
    public void addNewUser(String username, String password, String role, String email){
        if(username==null || username.isEmpty() || password == null || password.isEmpty()|| role == null || role.isEmpty()|| email == null || email.isEmpty()){
            return;
        }
       userDao.addUser(username, password, role, email);
    }

    /**
     * @param username username of the User
     * @return true if the username is within the database, false otherwise
     */
    public Boolean userExist(String username) {
        return userDao.checkUsernameExists(username);
    }

    /**
     * @param user User (Employee)
     * @param password new password for the User (Employee)
     */
    public void updatePassword(User user, String password) {
        userDao.updateUserPassword(user, password);
    }
}
