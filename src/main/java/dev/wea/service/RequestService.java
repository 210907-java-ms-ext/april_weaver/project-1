package dev.wea.service;

import dev.wea.data.RequestDao;
import dev.wea.model.Request;

import java.util.List;

public class RequestService {
    private RequestDao requestDao = new RequestDao();

    /**
     * @return list of requests
     */
    public List<Request> getAllRequests(){
        return requestDao.getRequests();
    }

    /**
     * @param employee_id id of the User (Employee) that submitted the Request
     * @param subject subject of the Request
     * @param description description of the Request
     */
    public void addNewRequest(int employee_id, String subject, String description){
        if(employee_id==0 || description == null || description.isEmpty()){
            return;
        }
        requestDao.addRequest(employee_id, subject, description);
    }

    /**
     * @param req_id id of the Request
     * @param state state of the Request (Pending, Approved, or Denied)
     * @param manager id of the User (Manager)
     */
    public void completeRequest(int req_id, String state, int manager) {
        requestDao.updateStateOfRequest(req_id, state, manager);
    }

    /**
     * @param id id of the User (Employee)
     * @return list of the Requests that the USer submitted
     */
    public List<Request> getRequestsByID(int id) {
        return requestDao.getRequestsID(id);
    }
}
