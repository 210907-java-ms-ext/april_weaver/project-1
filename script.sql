drop table if exists project1_user cascade;

create table project1_user (

user_id serial,
un varchar(100),
pw varchar(100),
user_role varchar(10),
email varchar(100),
primary key (user_id)

);

drop table if exists request;

create table request (

req_id serial,
employee_id int not null,
description text,
complete varchar(20),
manager_id int,
subject text,
primary key (req_id),
foreign key (employee_id) references project1_user(user_id) on delete cascade

);

select * from project1_user;

insert into project1_user (un, pw, user_role, email)
	values
	('hello', 'hi', 'Employee', 'hello@email.com'),
	('goodbye', 'bye', 'Manager', 'goodbye@email.com'),
	('manager', 'test', 'Manager', 'manager@email.com');

select * from request;

insert into request (employee_id, description, complete, manager_id, subject)
	values
	(1, 'This is hello.', 'Approved', 2, 'Hello'),
	(1, 'Deny this request.', 'Denied', 2, 'Another One'),
	(1, 'Testing Request', 'Pending', 0, 'Request');