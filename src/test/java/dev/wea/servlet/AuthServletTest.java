package dev.wea.servlet;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

class AuthServletTest extends Mockito {
    private HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    private HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    /**
     * tests the doPost method
     *
     * @throws Exception
     */
    @Test
    void doPost() throws Exception {
        when(request.getParameter("username")).thenReturn("hello");
        when(request.getParameter("password")).thenReturn("hi");

        new AuthServlet().doPost(request, response);

        verify(request, atLeast(1)).getParameter("username");
        verify(request, atLeast(1)).getParameter("password");
        verify(response, atLeast(1)).setStatus(200);
    }
}