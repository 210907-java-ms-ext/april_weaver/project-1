package dev.wea.servlet;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.atLeast;

class RegisterServletTest {
    private HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    private HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    /**
     * tests the doPost method
     *
     * @throws Exception
     */
    @Test
    void doPost() throws Exception {
        when(request.getParameter("username")).thenReturn("Test");
        when(request.getParameter("email")).thenReturn("Test");
        when(request.getParameter("password")).thenReturn("Test");
        when(request.getParameter("role")).thenReturn("Test");

        new RegisterServlet().doPost(request, response);

        verify(request, atLeast(1)).getParameter("username");
        verify(request, atLeast(1)).getParameter("email");
        verify(request, atLeast(1)).getParameter("password");
        verify(request, atLeast(1)).getParameter("role");
        verify(response, atLeast(1)).setStatus(200);
    }
}