package dev.wea.service;

import dev.wea.data.UserDao;
import dev.wea.model.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {

    UserService tempService = new UserService();

    /**
     * tests the getAllEmployees method
     */
    @Test
    void getAllEmployees() {
        assertEquals(tempService.getAllEmployees(), new UserDao().getEmployees());
    }

    /**
     * tests the getUserByCredentials method
     */
    @Test
    void getUserByCredentials() {
        User temp = tempService.getUserByCredentials("hello", "hi");
        assertEquals(temp, new UserDao().getUserByUsernameAndPassword("hello", "hi"));
    }

    /**
     * tests the getUserById method
     */
    @Test
    void getUserById() {
        User temp = tempService.getUserById(1);
        assertEquals(temp, new UserDao().getUserById(1));
    }

    /**
     * tests the uerExist method
     */
    @Test
    void userExist() {
        Boolean temp1 = tempService.userExist("hello");
        Boolean temp2 = tempService.userExist("asfasdfad");

        assertAll(
                ()->assertTrue(temp1),
                ()->assertFalse(temp2)
        );
    }
}